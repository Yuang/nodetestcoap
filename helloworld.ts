interface Person {
    firstName: string;
    LastName: string;
}

function greeter(person: Person) {
    return "Hello, " + person.firstName + " " + person.LastName;
}

// let user = "Jane User";

// let user = [1, 2, 3];
let user = {firstName: "Jane", LastName: "User"};

document.body.innerHTML = greeter(user);